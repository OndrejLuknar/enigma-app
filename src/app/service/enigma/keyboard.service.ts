import { Injectable } from '@angular/core';
import {KeyboardLine} from '../../entity/enigma/keyboard-line';

@Injectable()
export class KeyboardService {

  private _keyboard: KeyboardLine[];

  constructor() {
    this.init();
  }

  private init(): void {
    this._keyboard =
      [
        {line: ['Q', 'W', 'E', 'R', 'T', 'Z', 'U', 'I', 'O']},
        {line: ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K']},
        {line: ['P', 'Y', 'X', 'C', 'V', 'B', 'N', 'M', 'L']}
      ];
  }

  get keyboard() {
    return this._keyboard;
  }
}
