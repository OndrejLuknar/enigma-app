import {Component, Input, OnInit} from '@angular/core';
import {KeyboardService} from '../../../service/enigma/keyboard.service';
import {KeyboardLine} from '../../../entity/enigma/keyboard-line';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.css']
})
export class KeyboardComponent implements OnInit {

  keyboard: KeyboardLine[];

  @Input() selectKey: BehaviorSubject<string>;

  @Input() lightOn: Boolean = false;

  constructor(private keyboardService: KeyboardService) {

  }

  ngOnInit() {
    this.initKeyboard();
  }

  private initKeyboard(): void {
    this.keyboard = this.keyboardService.keyboard;
  }

}
