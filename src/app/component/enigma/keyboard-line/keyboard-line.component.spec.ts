import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyboardLineComponent } from './keyboard-line.component';

describe('KeyboardLineComponent', () => {
  let component: KeyboardLineComponent;
  let fixture: ComponentFixture<KeyboardLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeyboardLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyboardLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
