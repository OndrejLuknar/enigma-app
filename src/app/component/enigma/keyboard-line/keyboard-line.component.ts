import {Component, Input, OnInit} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-keyboard-line',
  templateUrl: './keyboard-line.component.html',
  styleUrls: ['./keyboard-line.component.css']
})
export class KeyboardLineComponent implements OnInit {

  @Input() line: string[];

  @Input() selectKey: BehaviorSubject<string>;

  @Input() lightOn: Boolean;

  constructor() { }

  ngOnInit() {
  }

  addAlphabet(key: string): void {
    this.selectKey.next(key);
  }

  lightKey(key: string): Boolean {
    return this.lightOn && this.selectKey.getValue() === key;
  }
}
