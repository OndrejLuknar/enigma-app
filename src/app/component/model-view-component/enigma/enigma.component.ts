import { Component, OnInit } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-enigma',
  templateUrl: './enigma.component.html',
  styleUrls: ['./enigma.component.css']
})
export class EnigmaComponent implements OnInit {

  selectLight: BehaviorSubject<string> = new BehaviorSubject<string>('');
  selectKey: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor() { }

  ngOnInit() {
    this.selectKey.subscribe(value => this.selectLight.next(value));
  }

}
