import {Plug} from './plug';

export interface Plugboard {
  plugs: Plug[];
}
