import {InnerCircle} from './inner-circle';
import {OuterCircle} from './outer-circle';

export interface Rotor {
  innerCircle: InnerCircle;
  outerCircle: OuterCircle;
}
