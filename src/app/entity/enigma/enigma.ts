import {Plugboard} from './plugboard';
import {Reflector} from './reflector';
import {Rotor} from './rotor';

export interface Enigma {
  plugboard?: Plugboard;
  reflector?: Reflector;
  rotors: Rotor[];
  message: string;
  codeMessage: string;
}
