import {Plug} from './plug';

export interface Reflector {
  reflection: Plug[];
}
