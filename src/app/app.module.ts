import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EnigmaComponent } from './component/model-view-component/enigma/enigma.component';
import { KeyboardComponent } from './component/enigma/keyboard/keyboard.component';
import {KeyboardService} from './service/enigma/keyboard.service';
import { KeyboardLineComponent } from './component/enigma/keyboard-line/keyboard-line.component';

@NgModule({
  declarations: [
    AppComponent,
    EnigmaComponent,
    KeyboardComponent,
    KeyboardLineComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [KeyboardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
